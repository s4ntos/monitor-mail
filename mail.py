import httplib2
from optparse import OptionParser
import os
import oauth2client
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import run_flow
import base64
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from apiclient import errors, discovery
import mimetypes
from email.mime.image import MIMEImage
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
import time  
from watchdog.observers import Observer  
from watchdog.events import PatternMatchingEventHandler  
import pprint 
import subprocess

SCOPE = 'https://www.googleapis.com/auth/gmail.send'
CLIENT_SECRET = '/home/xbian/monitor_mail/client_secret.json'
APPLICATION_NAME = 'Gmail API Python Send Email'
STORAGE = Storage('/home/xbian/monitor_mail/credentials.storage')

    
def get_credentials():
    # Fetch credentials from storage
    credentials = STORAGE.get()
    # If the credentials doesn't exist in the storage location then run the flow
    if credentials is None or credentials.invalid:
        flow = flow_from_clientsecrets(CLIENT_SECRET, scope=SCOPE)
        http = httplib2.Http()
        credentials = run_flow(flow, STORAGE, http=http)
    return credentials

def SendMessage(sender, attachmentFile=None):
    credentials = get_credentials()
    service = discovery.build('gmail', 'v1', credentials=credentials)
    subject = "Monitor someone at home"
    msgHtml = "Hi<br/>There is someone at home"
    msgPlain = "Hi\nThere is someone at home"
    if attachmentFile:
        message1 = createMessageWithAttachment(sender, sender, subject, msgHtml, msgPlain, attachmentFile)
    else: 
        message1 = CreateMessageHtml(sender, sender, subject, msgHtml, msgPlain)
    result = SendMessageInternal(service, "me", message1)
    return result
   
def SendMessageInternal(service, user_id, message):
    try:
        message = (service.users().messages().send(userId=user_id, body=message).execute())
        print('Message Id: %s' % message['id'])
        return message
    except errors.HttpError as error:
        print('An error occurred: %s' % error)
        return "Error"
    return "OK"

def CreateMessageHtml(sender, to, subject, msgHtml, msgPlain):
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = to
    msg.attach(MIMEText(msgPlain, 'plain'))
    msg.attach(MIMEText(msgHtml, 'html'))
    return {'raw': base64.urlsafe_b64encode(msg.as_string())}

def createMessageWithAttachment(
    sender, to, subject, msgHtml, msgPlain, attachmentFile):
    """Create a message for an email.

    Args:
    sender: Email address of the sender.
    to: Email address of the receiver.
    subject: The subject of the email message.
    msgHtml: Html message to be sent
    msgPlain: Alternative plain text message for older email clients          
    attachmentFile: The path to the file to be attached.

    Returns:
    An object containing a base64url encoded email object.
    """
    message = MIMEMultipart('mixed')
    message['to'] = to
    message['from'] = sender
    message['subject'] = subject
    messageA = MIMEMultipart('alternative')
    messageR = MIMEMultipart('related')
    messageR.attach(MIMEText(msgHtml, 'html'))
    messageA.attach(MIMEText(msgPlain, 'plain'))
    messageA.attach(messageR)
    message.attach(messageA)
    print("create_message_with_attachment: file: %s" % attachmentFile)
    content_type, encoding = mimetypes.guess_type(attachmentFile)
    if content_type is None or encoding is not None:
        content_type = 'application/octet-stream'
    main_type, sub_type = content_type.split('/', 1)
    if main_type == 'text':
        fp = open(attachmentFile, 'rb')
        msg = MIMEText(fp.read(), _subtype=sub_type)
        fp.close()
    elif main_type == 'image':
        fp = open(attachmentFile, 'rb')
        msg = MIMEImage(fp.read(), _subtype=sub_type)
        fp.close()
    elif main_type == 'audio':
        fp = open(attachmentFile, 'rb')
        msg = MIMEAudio(fp.read(), _subtype=sub_type)
        fp.close()
    else:
        fp = open(attachmentFile, 'rb')
        msg = MIMEBase(main_type, sub_type)
        msg.set_payload(fp.read())
        fp.close()
    filename = os.path.basename(attachmentFile)
    msg.add_header('Content-Disposition', 'attachment', filename=filename)
    message.attach(msg)
    return {'raw': base64.urlsafe_b64encode(message.as_string())}
        
class FileMailHandler(PatternMatchingEventHandler):
    patterns = ["*.mpg","*.avi"]
    def mail(self, mail):
        self.mail = mail
    def on_any_event(self, event):
        if not event.is_directory and event.event_type == 'modified':
            returncode = subprocess.call(["fuser",event.src_path])
            if returncode == 1:
                info = os.stat(event.src_path)
                SendMessage(self.mail, event.src_path)

def main():
    parser = OptionParser()
    parser.add_option("-m", dest="mail", default = "johndoe@johndoe.com",
                              help="Email to use")
    parser.add_option("-d", dest="directory", type="string", default = "/var/lib/motion",
                              help="Directory to monitor")
    (options, args) = parser.parse_args()
    event_handler = FileMailHandler()
    event_handler.mail(options.mail)
    observer = Observer()
    observer.schedule(event_handler, path=options.directory, recursive=True)
    observer.start()

    to = options.mail
    sender = options.mail
    try:
       while True:
             time.sleep(1)
    except KeyboardInterrupt:
       observer.stop()
    observer.join()

if __name__ == '__main__':
    main()
